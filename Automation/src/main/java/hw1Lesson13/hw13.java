package hw1Lesson13;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); // get the instance of Java Scanner which reads input from the use
        System.out.println("Enter your phone number");
        phoneValidation();
    }
    private static void phoneValidation() {
        Scanner value = new Scanner(System.in);
        String number = value.next();
        if (number.length() >= 10 && number.length() <= 13 && isCorrect(number)) {
            System.out.println("The Phone Number is correct");
            return;
        }
        System.out.println("The Phone Number must include 9-13 symbols");
        phoneValidation();
    }

    private static Boolean isCorrect(String data) {
        for (char aChar : data.toCharArray()) {
            while (!Character.isDigit(aChar)) {
                return false;
            }
        }
        return true;
    }
}
